# Waveshare NFC Eink Writer

Write image to https://www.waveshare.com/4.2inch-nfc-powered-e-paper.htm or https://www.waveshare.com/product/7.5inch-nfc-powered-e-paper.htm

## Run

`DEBUG=wne node index 4.2 image.png` or `DEBUG=wne node index 7.5 image.png`

Requires node 8.x

## Protocol

I didn't really figure out the protocol, I just ported what was being done in the Android app ("NFCTag") from the Waveshare wiki.

### Browser.c from ST25R3911B-NFC-Demo

The source code for the ST25R3911B programmer is available on the waveshare wiki and offers more insight into the commands used:

```
uint8_t						step1[3]={0xcd,0x00,10};				//select e-paper type and reset e-paper  						4:2.13inch e-Paper   7:2.9inch e-Paper  10:4.2inch e-Paper  14:7.5inch e-Paper
uint8_t						step2[2]={0xcd,0x01};						//e-paper normal mode  type£º
uint8_t						step3[2]={0xcd,0x02};						//e-paper config1
uint8_t						step4[2]={0xcd,0x03};						//e-paper power on
uint8_t						step5[2]={0xcd,0x05};						//e-paper config2
uint8_t						step6[2]={0xcd,0x06};						//EDP load to main
uint8_t						step7[2]={0xcd,0x07};						//Data preparation
uint8_t						step8[123]={0xcd,0x08,0x64};		//Data start command			2.13inch(0x10:Send 16 data at a time)    2.9inch(0x10:Send 16 data at a time)     4.2inch(0x64:Send 100 data at a time)  7.5inch(0x78:Send 120 data at a time)
uint8_t						step9[2]={0xcd,0x18};					//e-paper power on
uint8_t						step10[2]={0xcd,0x09};					//Refresh e-paper
uint8_t						step11[2]={0xcd,0x0a};					//wait for ready
uint8_t						step12[2]={0xcd,0x04};					//e-paper power off command
//	uint8_t						step13[2]={0xcd,0x0b};					//Judge whether the power supply is turned off successfully
//	uint8_t						step14[2]={0xcd,0x0c};					//The end of the transmission

```
